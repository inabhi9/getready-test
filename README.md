GetReady Test
=============

Problem statement
------------------
Scenario: You work for an Internet of Things company that produces automated devices for gathering weather data. These devices are connected to the Internet and send data to a central server on a regular schedule. Your job is to design a simple RESTful API (resources and representations) for the central server that these devices communicate with, write the server software that implements the API.

The devices communicate with the server using HTTP. Their requests will contain two pieces of data:

* The unique ID of the device, and the current temperature at the device’s location.
* The server should record these data (in a MongoDB database) along with the date and time that the request was received.

The API should also provide a means for a user to get a list of data that have been received by the server.

> Note: For the purposes of this exercise, don’t worry about security. Assume that this is a completely open service without authentication or authorization.


Technology Used
---------------

* Python 2.7.x
* MongoDB
* Tornado


How to Run
----------
Running this application is very straight forward.

On your machine install requirements using `pip`

    pip install -r requirement.txt
    
After finishing the installment of requirements, set the environment variables for the application.
Supported environment variables are

* **GR_MONGODB_URL**: Mongodb connection URL without database name. Default `mongodb://localhost:27017/`
* **GR_MONGODB_NAME**: Database name. Default `getready_test`
* **GR_PORT**: API server listening port. Default `4533`

Then simply run

    python src/run.py

> Note: You must have mongodb up and running

or if you have have docker installed simply run

    
    docker pull registry.gitlab.com/inabhi9/getready-test
    docker run -p 4533:4533 registry.gitlab.com/inabhi9/getready-test:latest
    
This docker image has built-in mongodb installed and you will be able to access
server from all interfaces

Demo
----
Currently this test demo is hosted at

    http://getready-test.abhi9.in:1000


Available endpoints
-------------------

Please import `getready.postman_collection.json` in postman to test/see available endpoints
