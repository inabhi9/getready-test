FROM mongo:3

RUN apt-get update
RUN apt-get install -y python python-pip supervisor

RUN mkdir -p /usr/src/getready
WORKDIR /usr/src/getready

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . ./

COPY docker/supervisor.conf /etc/supervisor/conf.d/program.conf

EXPOSE 4533
ENTRYPOINT []
CMD ["supervisord", "-n"]
