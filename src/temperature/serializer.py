from core.errors import ValidationError


class TemperatureSerializer:
    data = None

    def __init__(self, data):
        self._data = data or {}

    def is_valid(self):
        device_id = self._data.get('device_id')
        temperature = self._data.get('temperature')
        self.data = {}

        # Require validation
        if not device_id:
            raise ValidationError('Device id is required')

        if not temperature:
            raise ValidationError('Temperature is required')

        # Type validation
        try:
            self.data['device_id'] = str(device_id)
        except (ValueError, TypeError):
            raise ValidationError('Device id must be a type of string')

        try:
            self.data['temperature'] = float(temperature)
        except (ValueError, TypeError):
            raise ValidationError('Temperature must be a type of float')
