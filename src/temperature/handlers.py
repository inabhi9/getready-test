from core.base_request_handler import BaseRequestHandler
from core.decorators import route
from core.errors import ValidationError
from serializer import TemperatureSerializer


@route(r"/temperature")
class TemperatureHandler(BaseRequestHandler):
    PAGE_SIZE = 10

    def data_received(self, chunk):
        pass

    def get(self, *args, **kwargs):
        coll = self.application.db.temperature
        total_count = coll.count()
        page_meta = self.get_pagination_meta(total_count)
        offset = page_meta.pop('offset')
        filter_device_id = self.request.query_arguments.get('device_id')

        # Find query
        fq = {}
        if filter_device_id and filter_device_id[0]:
            fq['device_id'] = filter_device_id[0]

        data = coll.find(fq).skip(offset).limit(self.PAGE_SIZE)
        meta = page_meta

        self.set_header('Access-Control-Allow-Origin', '*')
        self.write({'data': data, 'meta': meta})

    def post(self, *args, **kwargs):
        serializer = TemperatureSerializer(data=self.json_body)

        try:
            serializer.is_valid()
        except ValidationError, e:
            self.set_status(400)
            self.write({'error': {'message': e.message, 'type': e.__class__.__name__}})
            return

        data = serializer.data.copy()
        self.application.db.temperature.insert(data)

        # Ensure create happened
        if not data.get('_id'):
            self.write({'error': {'message': 'Cannot create', 'type': 'DatabaseError'}})

        self.set_status(201)
        self.set_header('Access-Control-Allow-Origin', '*')
        self.write({'data': data})
