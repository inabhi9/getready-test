import importlib
import os

INSTALLED_APP = (
    'core',
    'temperature'
)
for app in INSTALLED_APP:
    importlib.import_module(app)

MONGO_URL = os.environ.get('GR_MONGODB_URL') or 'mongodb://localhost:27017/'
MONGO_DB = os.environ.get('GR_MONGODB_NAME') or 'getready_test'
PORT = os.environ.get('GR_PORT') or 4533
