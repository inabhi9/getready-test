from pymongo import MongoClient
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application

import settings
from core.registry import HANDLERS


class Service:
    def __init__(self):
        self.db = self._get_new_connection()[settings.MONGO_DB]

    def start(self):
        http_server = HTTPServer(self._get_tornado_app())
        http_server.listen(settings.PORT, '0.0.0.0')
        IOLoop.instance().start()

    def _get_new_connection(self):
        """
        Makes connection to database
        :return MongoClient:
        """
        return MongoClient(settings.MONGO_URL)

    def _get_tornado_app(self):
        """
        Init Application object with handlers and other settings

        :return Application:
        """
        app = Application(
            handlers=HANDLERS,
            gzip=True
        )
        app.db = self.db

        return app
