import settings
from service import Service

if __name__ == '__main__':
    print 'API Server started on: 0.0.0.0:%s' % settings.PORT
    Service().start()
