from registry import HANDLERS


def route(pattern):
    def _decorate(cls):
        HANDLERS.append((pattern, cls))
        return cls

    return _decorate
