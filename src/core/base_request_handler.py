import json
import math
import re

from tornado.escape import utf8
from tornado.httputil import url_concat
from tornado.util import unicode_type
from tornado.web import RequestHandler

from core import json_util


class BaseRequestHandler(RequestHandler):
    PAGE_SIZE = 10

    @property
    def json_body(self):
        if self.request.headers.get('Content-Type') in ['application/json', 'text/json']:
            return json.loads(self.request.body)

    def write(self, chunk):
        """
        Overridden function to make a use of custom json encoder
        """
        if self._finished:
            raise RuntimeError("Cannot write() after finish()")
        if not isinstance(chunk, (bytes, unicode_type, dict)):
            message = "write() only accepts bytes, unicode, and dict objects"
            if isinstance(chunk, list):
                message += ". Lists not accepted for security reasons; " \
                           "see http://www.tornadoweb.org/en/stable/web.html#tornado.web.RequestHandler.write"
            raise TypeError(message)
        if isinstance(chunk, dict):
            chunk = json_util.dumps(chunk)
            self.set_header("Content-Type", "application/json; charset=UTF-8")
        chunk = utf8(chunk)
        self._write_buffer.append(chunk)

    def get_pagination_meta(self, total_count):
        query = self.request.query_arguments
        request_url = re.sub(r'page=[0-9]+', '', self.request.full_url())
        current_page = int((query.get('page') or [1])[0])
        total_page = math.ceil(float(total_count) / float(self.PAGE_SIZE))
        offset = (current_page - 1) * self.PAGE_SIZE
        _next = None
        _prev = None

        if current_page < total_page:
            _next = url_concat(request_url, {'page': current_page + 1})

        if current_page > 1:
            _prev = url_concat(request_url, {'page': current_page - 1})

        return {
            'offset': offset,
            'next': _next,
            'previous': _prev,
            'count': total_count
        }
